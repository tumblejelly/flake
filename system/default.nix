{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./boot
    ./fs.nix
    ./graphics
    ./novidia.nix
    ./sound.nix
    ./fonts.nix
  ];

  networking.hostName = "nixos";
  networking.networkmanager.enable = true;

  time.timeZone = "Europe/Moscow";
  i18n.defaultLocale = "en_GB.UTF-8";

  users.users.jelly = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "input" "adbusers" ];
  };

  security.polkit.enable = true;
  # xdg.portal = {
  #   enable = true;
  #   extraPortals = with pkgs; [
  #     xdg-desktop-portal-gnome
  #     xdg-desktop-portal-hyprland
  #     # xdg-desktop-portal-gtk
  #   ];
  # };

  services.fwupd.enable = true;
  services.gnome.gnome-keyring.enable = true;
  services.printing.enable = true;
  services.flatpak.enable = true;

  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    git
    vim
    wget
    curl
    unzip
    tmux
    tree
    killall
    file
    brightnessctl
    flatpak-builder
    steam-run
    virtiofsd
  ];

  programs.adb.enable = true;

  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;

  environment.variables.EDITOR = "vim";

  documentation.nixos.enable = false;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  system.stateVersion = "23.11";
}

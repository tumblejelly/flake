{ pkgs, ... }:

{
  imports = [ ./secureboot.nix ];
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    loader = { 
      systemd-boot.enable = true;
      systemd-boot.editor = false;
      efi.canTouchEfiVariables = true;
    };

    initrd.systemd.enable = true;

    plymouth = {
      enable = true;
      theme = "connect";
      themePackages = with pkgs; [
        (adi1090x-plymouth-themes.override { selected_themes = [ "connect" ]; })
      ];
    };

    # quiet boot
    loader.timeout = 0;
    consoleLogLevel = 0;
    initrd.verbose = false;
    kernelParams = [
      "quiet"
      "intel_iommu=on"
    ];
  };
}

{ pkgs, ... }:

{
  programs.hyprland.enable = true;

  security.pam.services.swaylock = {};

  environment.systemPackages = with pkgs; [
    waybar
    rofi-wayland
    dunst
    swayidle
    swaylock
  ];
}

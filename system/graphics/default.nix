{ pkgs, ... }:

{
  imports = [
    ./gnome.nix
    ./hyprland.nix
  ];

  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    excludePackages = [ pkgs.xterm ]; 
  };

  environment.systemPackages = with pkgs; [
    alacritty
    wl-clipboard
  ];
}

{ pkgs, ... }:

{
  services.xserver.desktopManager.gnome.enable = true;

  environment.gnome.excludePackages = (with pkgs; [
    gnome-tour
    yelp
    gnome-connections
    evince
    loupe
    gnome-text-editor
    epiphany
    snapshot
  ]) ++ (with pkgs.gnome; [
    gnome-weather
    gnome-maps
    totem
    simple-scan
    gnome-logs
    file-roller
    gnome-calendar
  ]);

  environment.systemPackages = with pkgs.gnome; [
    gnome-software
    gnome-tweaks
  ];
}

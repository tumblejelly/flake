{ pkgs, ... }:

{
  programs.neovim.enable = true;
  programs.neovim.plugins = with pkgs.vimPlugins.nvim-treesitter-parsers; [
    blueprint
    sql
    hyprlang
    lua
  ];
  xdg.configFile.nvim.source = ./nvim;
}

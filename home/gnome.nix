{ lib, ... }:

let
  mkTuple = lib.hm.gvariant.mkTuple;
  mkUint32 = lib.hm.gvariant.mkUint32;
in {
  dconf.settings = {
    # multitasking
    "org/gnome/mutter" = {
      edge-tiling = true;
      dynamic-workspaces = true;
    };
    "org/gnome/shell/app-switcher" = { current-workspace-only = true; };

    # idle
    "org/gnome/desktop/session" = { idle-delay = mkUint32 600; };
    "org/gnome/settings-daemon/plugins/power" = {
      idle-dim = false;
      sleep-inactive-battery-type = "nothing";
      sleep-inactive-ac-type = "nothing";
    };
    "org/gnome/desktop/screensaver" = {
      lock-enabled = true;
      lock-delay = mkUint32 0;
    };

    "org/gnome/settings-daemon/plugins/power" = {
      power-saver-profile-on-low-battery = false;
    };

    # visuals
    "org/gnome/desktop/interface" = {
      show-battery-percentage = true;
      clock-show-weekday = true;
      text-scaling-factor = 1.25;
    };

    # touchpad
    "org/gnome/desktop/peripherals/touchpad" = {
      speed = 0.30;
      tap-to-click = true;
    };
    "org/gnome/desktop/interface" = {
      gtk-enable-primary-paste = false;
    };

    # keyboard
    "org/gnome/desktop/wm/keybindings" = {
      close = [ "<Super>q" ];
    };
    "org/gnome/desktop/input-sources" = {
      xkb-options = [ "ctrl:nocaps" "shift:both_capslock" ];
    };

    # i18n
    "org/gnome/desktop/input-sources" = {
      sources = [
        (mkTuple [ "xkb" "us" ])
        (mkTuple [ "xkb" "ru" ])
      ];
    };
    "system/locale".region = "en_GB.UTF-8";

    "org/gnome/software" = {
      download-updates = false;
      download-updates-notify = false;
    };
  };
}

{ config, pkgs, lib, ... }:

{
  home.username = "jelly";
  home.homeDirectory = "/home/jelly";

  imports = [
    ./flatpak.nix
    ./gnome.nix
    ./fusuma # TODO
    ./nextcloud.nix
    ./neovim.nix
  ];

  home.packages = with pkgs; [
    fastfetch
    godot_4

    jdk

    gcc

    clang-tools
    nil
    lua-language-server

    corefonts
  ];

  fonts.fontconfig.enable = true;

  home.pointerCursor = {
    name = "Adwaita";
    package = pkgs.gnome.adwaita-icon-theme;
    size = 24;
    x11 = {
      enable = true;
      defaultCursor = "Adwaita";
    };
  };

  programs.home-manager.enable = true;
  home.stateVersion = "23.11";
}

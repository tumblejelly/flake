vim.lsp.set_log_level('debug')

_G.meson_recommended_style = false

-- Some language servers have issues with backup files
vim.opt.backup = false
vim.opt.writebackup = false

-- lower delays
vim.opt.updatetime = 300

local capabilities = require('cmp_nvim_lsp').default_capabilities()
local lspconfig = require('lspconfig')
local luasnip = require('luasnip')
local cmp = require('cmp')

-- integrate lsp with cmp
local servers = {
    'clangd',
--  'rust_analyzer',
    'pyright',
    'nil_ls',
    'lua_ls',
    'asm_lsp',
    'blueprint_ls',
    'gdscript',
}
for _, lsp in ipairs(servers) do
    lspconfig[lsp].setup {
        capabilities = capabilities,
    }
end

-- setup neodev for nvim configs
require('neodev').setup()
-- setup lua_ls
lspconfig.lua_ls.setup {
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' },
            },
            telemetry = { enable = false },
        }
    }
}

require('typescript-tools').setup {}

local dap = require('dap')

dap.adapters.gdb = {
    type = 'executable',
    command = 'gdb',
    args = { '-i', 'dap' },
}

dap.adapters.lldb = {
	type = "executable",
	command = "/usr/bin/lldb-vscode", -- adjust as needed
	name = "lldb",
}

local gdb = {
    name = "Launch",
    type = "gdb",
    request = "launch",
    program = function()
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = "${workspaceFolder}",
    stopAtBeginningOfMainSubprogram = false,
    runInTerminal = true,
}

local lldb = {
    name = 'Launch',
    type = 'lldb',
    request = 'launch',
    program = function()
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
    args = {},
    runInTerminal = true,
}

dap.configurations.asm = {
    gdb
}

require('dapui').setup()

-- setup cmp
cmp.setup {
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'nvim_lsp_signature_help' },
        { name = 'luasnip' },
        { name = 'path' },
    },
}

require('lsp_signature').setup()

-- setup treesitter
require('nvim-treesitter.configs').setup {
    ensure_installed = {
        'blueprint', 'sql', 'hyprlang'
    },
    auto_install = false;
    highlight = {
        enable = true,
        disable = { 'lua' },
    },
}

-- hyprlang
vim.filetype.add({
    pattern = { [".*/hyprland%.conf"] = "hyprlang" },
})

-- require('nvim-treesitter.configs').setup {
--     refactor = {
--         smart_rename = {
--             enable = true,
--             keymaps = { smart_rename = '<leader>rn' },
--         },
--         highlight_definitions = {
--             enable = true,
--             clear_on_cursor_move = true,
--         },
--     }
-- }

require('gitsigns').setup()

require('trouble').setup()

require('nvim-autopairs').setup {
    map_bs = true,
    map_cr = true,
}

vim.api.nvim_create_autocmd(
    'CursorHold',
    {
        callback = function()
            vim.diagnostic.open_float({focus=false})
        end,
    }
)

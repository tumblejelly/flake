{ pkgs, ... }:

{
  nixpkgs.overlays = [
    (final: prev: {
      fusuma = prev.fusuma.override {
        bundlerApp = prev.bundlerApp.override {
          gemdir = ./.;
        };
      };
    })
  ];
  home.packages = with pkgs; [ fusuma ];

  services.fusuma = {
    enable = true;
    settings = {
      swipe."4" = {
        left.sendkey = "LEFTCTRL+TAB";
        right.sendkey = "LEFTCTRL+LEFTSHIFT+TAB";
        up.sendkey = "LEFTALT+RIGHT";
        down.sendkey = "LEFTALT+LEFT";
      };
      hold."4" = {
        sendkey = "LEFTCTRL+w";
        threshold = 0.2;
      };
    };
  };
}

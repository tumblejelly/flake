{ ... }:

{
  services.flatpak.packages = [
    "org.mozilla.firefox"
    "org.libreoffice.LibreOffice"
    "org.blender.Blender"
    "org.kde.krita"
    "org.telegram.desktop"
    "org.videolan.VLC"
    "com.valvesoftware.Steam"
    "com.mattjakeman.ExtensionManager"
    "app.drey.Warp"
    "org.gnome.Loupe"
    "org.gnome.Calendar"
    "org.gnome.SimpleScan"
    "org.gnome.Evince"
    "org.gnome.TextEditor"
    "org.gnome.FileRoller"
    "org.gnome.Snapshot"
    "org.gnome.World.Secrets"
    "com.github.tenderowl.frog"
    "de.haeckerfelix.Fragments"
    "net.nokyan.Resources"
    "io.gitlab.adhami3310.Impression"
  ];
}
